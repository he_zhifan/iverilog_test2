`timescale 1ns/1ps
module test2_tb; 
reg clk,rst_n;
wire [3:0] cnt;
wire acc;

initial begin 
	rst_n = 1;
	clk =0;
	#10 rst_n = 0;
	#10 rst_n = 1;
	#500 $finish;
end

always #5 clk = ~clk;

test testop(
	.clk(clk),
	.rst_n(rst_n), 
	.acc(acc),
	.cnt(cnt));
    
/* for iverilog
initial begin
	$dumpfile("test2.vcd"); 
	$dumpvars; 
end
*/
/* for vcs
initial begin
    $vcdpluson();
end
*/

// for verdi
/*
initial begin
  $fsdbDumpfile("test2.fsdb");
  $fsdbDumpvars;
  $fsdbDumpon();
end
*/
endmodule
