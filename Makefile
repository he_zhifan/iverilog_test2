vcs:
        vcs -full64 -f verilog_file.f -debug_pp +vcd+vcdpluson
        ./simv
        dve -vpd vcdplus.vpd
verdi:
        vcs -full64 -f verilog_file.f -kdb -debug_access+all -lca -l comp.log
        ./simv
        verdi -ssf test2.fsdb -arch64 -nologo &

iverilog:
        iverilog test2.v test2_tb.v
        ./a.out
        gtkwave test2.vcd

clean: 
        -rm -rf csrc *.fsdb *.log verdi* simv* ucli* novas* fontconfig DVEfiles  *.vpd
