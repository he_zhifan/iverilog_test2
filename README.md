# iverilog_test2

## iverilog
add this to test2_tb.v

```verilog
initial begin
$dumpfile("test2.vcd")
$dumpvars;
end
```

run:
```bash
iverilog test2_rtl.v test2_tb.v
./a.out
gtkwave test2.vcd
```

## vcs
add this to test2_tb.v
```verilog
initial begin
    $vcdpluson();
end
```

run

```
vcs -full64 -f verilog_file.f -debug_pp +vcd+vcdpluson
./simv
dve -vpd vcdplus.vpd
```
## verdi
add this to test2_tb.v
```
initial begin
  $fsdbDumpfile("test2.fsdb");
  $fsdbDumpvars;
  $fsdbDumpon();
end
```


```
vcs -full64 -f file2.f -kdb -debug_access+all -lca -l comp.log
./simv
verdi -ssf test2.fsdb -arch64 -nologo &
```

