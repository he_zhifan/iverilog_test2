`timescale 1ns/1ps
module test(
	input  clk,rst_n,
	output reg acc,
	output reg [3:0] cnt);
	reg enble;

	always @(posedge clk or negedge rst_n) begin
	if(~rst_n)
		acc <= 0;
	else 
		acc = ~acc;
	end

	always @(posedge clk or negedge rst_n) begin
	if(~rst_n)
		enble <= 0;
	else 
		enble <= ~enble;
	end	

	always @(posedge clk or negedge rst_n) begin
	if (~rst_n)
		cnt <= 0;
	else if(enble)	
		cnt <= cnt + acc;
	end
		
endmodule
